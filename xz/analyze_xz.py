from binascii import crc32

MAGIC_BYTES_HEADER = "fd377a585a00"
MAGIC_BYTES_FOOTER = "595a"

# Non-reserved header flags
VALID_FLAGS_MASK = ["0000", "0001", "0004", "000a"]

def xz(file : bytes):

    # Keeping track of multiple streams ...    
    header_flags = []
    footer_flags = []
    offset = -1
    
    for i in range(len(file)-1):
        if file[i:i+6].hex() == MAGIC_BYTES_HEADER and i < len(file)-12: # Check for magic number

            # Verify header flags
            if file[i+6:i+8].hex() not in VALID_FLAGS_MASK:
                if len(header_flags) == len(footer_flags):
                    break   # File is malformed (or contains trailing bytes)
            # Continue because magic number most likely located in the block area

            header_checksum = crc32(bytes.fromhex(file[i+6:i+8].hex()))
            checksum = int(file[i+8:i+12][::-1].hex(),16)   # convert from big to little endian

            if header_checksum != checksum:
                if len(header_flags) == len(footer_flags):
                    break # File is malformed (or contains trailing bytes)

            # Add stream header
            header_flags.append(file[i+6:i+8].hex())

            if len(header_flags) > 1:        # Check for multiple streams
                n_bytes = i - offset
                if n_bytes % 4 != 0: break  # Padding bytes always multiple of 4s 
                zeros = '00' * n_bytes  # Stream padding is always 0s
                stream_padding = file[offset:i] 
                if stream_padding != zeros: break   # File is malformed (or contains trailing bytes)


        if file[i:i+2].hex() == MAGIC_BYTES_FOOTER and i > 24:
            # Verify that header and footer have same flags
            if file[i-2:i].hex() == header_flags[-1]:

                # Verify the checksum
                backward_size = file[i-6:i-2].hex()
                data = bytes.fromhex(backward_size + file[i-2:i].hex())
                footer_checksum = crc32(data)
                checksum = int(file[i-10:i-6][::-1].hex(),16)   # convert from big to little endian

                if footer_checksum != checksum: continue # likely not the footer but data
                
                # Add footer flags
                footer_flags.append(file[i-2:i].hex())

                # Set current offset
                offset = i + 2

    return offset