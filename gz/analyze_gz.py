from gzip import _GzipReader, BadGzipFile
import io

class Custom_GzipReader(_GzipReader):
    def get_number_of_trailing_bytes(self):
        '''
        Read the gzip archive and return the number of trailing bytes.
        Returns:
            -1 if very first gzip stream is malformed
             0 if there are no trailing bytes
             no. of trailing_bytes else
        '''
        members = 0
        number_of_trailing_bytes = 0

        while True:
            if self._decompressor.eof:
                # Ending case: we've come to the end of a member in the file,
                # so finish up this member, and read a new gzip header.
                # Check the CRC and file size, and set the flag so we read
                # a new member
                self._read_eof()
                self._new_member = True
                self._decompressor = self._decomp_factory(
                    **self._decomp_args)
                
                # save last valid number of trailing bytes (after EOF)
                number_of_trailing_bytes = self._fp._length

            if self._new_member:
                # If the _new_member flag is set, we have to
                # jump to the next member, if there is one.
                self._init_read()

                try:
                    # Try to read next gzip stream header 
                    # (multiple might be present in one file) 
                    # Returns None if no more data available to process.
                    gzip_header = self._read_gzip_header()
                except BadGzipFile:
                    # If it throws an exception, this means that there are trailing bytes
                    # that can't be parsed as another gzip stream
                    # However, if we cannot even parse the very first header, then the
                    # file is malformed and we don't want to output a number of trailing bytes.
                    if members > 0:
                        return number_of_trailing_bytes
                    else:
                        return -1
                
                if not gzip_header:
                    # No gzip header available, blank file, so no trailing bytes
                    return number_of_trailing_bytes
                
                self._new_member = False
                members += 1

            # Read a chunk of data from the file
            buf = self._fp.read(io.DEFAULT_BUFFER_SIZE)

            uncompress = self._decompressor.decompress(buf)
            if self._decompressor.unconsumed_tail != b"":
                self._fp.prepend(self._decompressor.unconsumed_tail)
            elif self._decompressor.unused_data != b"":
                # Prepend the already read bytes to the fileobj so they can
                # be seen by _read_eof() and _read_gzip_header()
                self._fp.prepend(self._decompressor.unused_data)

            if uncompress != b"":
                self._add_read_data( uncompress )
                self._pos += len(uncompress)
            if buf == b"":
                # Compressed file ended before the end-of-stream marker was reached
                if members > 0:
                    return number_of_trailing_bytes
                else:
                    return -1

def gz(file : bytes):
    gzr = Custom_GzipReader(io.BytesIO(file))
    number_of_trailing_bytes = gzr.get_number_of_trailing_bytes()

    if number_of_trailing_bytes != -1:
        return len(file) - number_of_trailing_bytes 
    else:
        return -1