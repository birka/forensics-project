import argparse
import sys
import magic

from sevenZip.analyze_7zip import sevenZip
from jpeg.analyze_jpeg import jpeg
from elf.analyze_elf import elf
from gz.analyze_gz import gz
from xz.analyze_xz import xz
from tar.analyze_tar import tar
from zip.analyze_zip import zip
from bzip2.analyze_bzip2 import bzip2

supported_mime_types = {
    "image/jpeg": jpeg,
    "image/png": "0000000049454e44ae426082",
    "application/x-tar": tar,
    "application/gzip": gz,
    "application/x-xz": xz,
    "application/x-bzip2": bzip2,
    "application/x-7z-compressed": sevenZip,
    "application/x-sharedlib": elf,
    "application/x-elf": elf,
    "application/x-executable": elf,
    "application/zip": zip,
    "application/x-zip-compressed": zip,
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document": zip,
    "application/vnd.openxmlformats-officedocument.presentationml.presentation": zip,
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": zip,
    "application/vnd.oasis.opendocument.text": zip,
    "application/vnd.oasis.opendocument.spreadsheet": zip,
    "application/vnd.oasis.opendocument.presentation": zip,
    "application/pdf": "2525454F46"
}


def analyze(filename, dump, outfile):
    with open(filename, 'rb') as f:
        file = f.read()
    
    mime_type = magic.from_buffer(file, mime=True)
    print("File MIME type: " + mime_type + "\n", file=sys.stderr)

    if mime_type in supported_mime_types:
        trailing_bytes_offset = -1

        if callable(supported_mime_types[mime_type]):
            trailing_bytes_offset = supported_mime_types[mime_type](file)
        else:
            # generic case, static footer
            magic_footer = bytes.fromhex(supported_mime_types[mime_type])
            for i in range(0, len(file), 1):
                if file[i:i+len(magic_footer)] == magic_footer:
                    # matching footer found
                    trailing_bytes_offset = i + len(magic_footer)
                    

        if trailing_bytes_offset == -1:
            print("Error: Malformed file. Found matching header for filetype, "
                    "but EOF indication was not found." ,file=sys.stderr)
        else:
            trailing_bytes = file[trailing_bytes_offset:]
            print(f"Number of trailing bytes: {len(trailing_bytes)}", file=sys.stderr)

            if len(trailing_bytes) != 0:
                if dump:
                    print("Dumping trailing bytes to console:", file=sys.stderr)
                    sys.stdout.buffer.write(trailing_bytes)
                if outfile:
                    print("Dumping trailing bytes to seperate output file " + outfile, file=sys.stderr)
                    with open(outfile, 'wb') as out:
                        out.write(trailing_bytes)
    else:
        print(f"Filetype MIME type is not supported!"
                " To list the supported filetypes"
                " use the '-i' option", file=sys.stderr)
                        
                        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Find and extract trailing bytes from files.'
        )

    parser.add_argument("-i", "--info", action="store_true",
                        help="Optional argument to list supported file types")

    args = parser.parse_known_args()[0]
    if args.info:
        info = "This program extracts trailing bytes from files iff present. Supported file MIME types:"
        for key in supported_mime_types:
            info +="\n\t" + key
        print(info, file=sys.stdout)
             
    else:
        parser.add_argument('-f', '--file', action='store', 
                            required=True, help='File to analyze')
        parser.add_argument('-d', '--dump', action='store_true',
                            help='Dump the trailing bytes to the console') 
        parser.add_argument('-o', '--outfile', action='store',
                            help='Dump the trailing bytes to a file') 

        args = parser.parse_args()

        analyze(args.file, args.dump,args.outfile)