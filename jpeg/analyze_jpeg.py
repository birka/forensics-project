def jpeg(file : bytes):
        # check for the number of headers
        header = 0
        # check for the number of footers
        footer = 0 
        idx = 0
        # Parse JPEG
        while idx < len(file)-1:
            byte = file[idx:idx+2].hex()
            if byte[:2] == 'ff':    # Read marker
                if byte[2:] == 'd9':  # end of image
                    footer += 1
                    # End of real image (not embedded)
                    if header == footer: break
                    idx += 2
                elif byte[2:] == '01':  # reserved marker do nothing
                    idx += 1
                elif byte[2:] == '00':
                    # not a marker, so skip it
                    idx += 1
                else:   # Read length of unimportant chunk and skip it
                    if byte[2:] == 'd8':  # Start of image
                        header += 1
                    length = int(file[idx+2:idx+4][::-1].hex(),16)
                    idx += length - 2
            else:
                idx += 1 # skip to next byte

        # trailing bytes offset
        return idx+2