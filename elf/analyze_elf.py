import sys
from io import BytesIO

from elftools.elf.elffile import ELFFile

def elf(file : bytes):
    elf = ELFFile(BytesIO(file))

    max_sec_end_offset = -1
    max_seg_end_offset = -1
    sht_end_offset = -1
    
    try: 
        # Determine where last section ends
        section_headers = [elf._get_section_header(i) for i in range(elf.num_sections())]
        max_sec_end_offset = max([sec['sh_offset'] + sec['sh_size'] for sec in section_headers])

        print(f"Last section ends at byte: {max_sec_end_offset}", file=sys.stderr)

        # Next, determine where section header table (SHT) ends
        # maybe SHT doesn't exist in reality but is defined in ELF header
        # for example if binary was extracted from memory

        # In this case, the calculation for the individual sections
        # above will already lead to an error and thus the
        # sht_end_offset will not be calculated
        sht_end_offset = elf['e_shoff'] + (elf['e_shentsize'] * elf['e_shnum'])
        print(f"Section Header Table ends at byte: {sht_end_offset}", file=sys.stderr)
    except: 
        print("Unable to read section headers!", file=sys.stderr)

    try: 
        # Determine where last segment ends
        segment_headers = [elf._get_segment_header(i) for i in range(elf.num_segments())]
        max_seg_end_offset = max([seg['p_offset'] + seg['p_filesz'] for seg in segment_headers])
        print(f"Last segment ends at byte: {max_seg_end_offset}", file=sys.stderr)
    except: 
        print("Unable to read segment headers!", file=sys.stderr)

    print("", file=sys.stderr)
        
    return max(max_sec_end_offset, max_seg_end_offset, sht_end_offset)
