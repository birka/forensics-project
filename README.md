# Forensics 2023 Project - Identifying and Extracting Trailing Bytes from files
Authors: Timm Birka, Manuel Peters

[TOC]
<!-- Note: append bytes to files using truncate -s +10 <filename> (does not work for hex dumps, +10 adds 10 0bytes, number is not important) -->
<!-- Create hexdump using xxd <filename> -->
<!-- Create file from hexdump using xxd -r <filename> -->

## Introduction
Trailing bytes are additional information that is appended to files after they already ended. This can be used to hide data as trailing bytes are usually ignored by programs. This project identifies trailing bytes for specific file formats, extracts them, and optionally dumps them to console or to a specified output file.

## Installation
To use this project, Python 3.9 or higher is recommended as this implementation was not tested with older versions. The required packages can be installed via the given `requirements.txt` file:
```
pip install -r requirements.txt
```
Note that for Windows, this will install the package python-magic-bin, which contains DLLs used to access the libmagic library to identify file types.

# Usage
```
python3 trailing_bytes.py [-h] [-i] -f FILE [-d] [-o OUTFILE]
```

The program specifies the following command line arguments:
```
-i: Shows the supported file MIME types
-f: Input file which will be analyzed for trailing bytes (required)
-d: Dump the trailing bytes to console (optional)
-o: Output file to write trailing bytes in (optional)
```

Currently, we support the analysis of the following file types:
-   ELF
-   JPEG
-   PNG
-   PDF
-   DOCX
-   PPTX
-   XLSX
-   ODT
-   ODS
-   ODX
-   TAR
-   GZ
-   XZ
-   BZ2
-   7ZIP
-   ZIP

If the argument `-d` is set, the trailing bytes will be dumped to console in binary format. Since the trailing bytes might not be displayable in ASCII, it is recommended to pipe the output using xxd to see the hex representation of the trailing bytes:
```
$ python3 trailing_bytes.py -f office/test.docx -d | xxd
```
which yields the following output:
```
File MIME type: application/vnd.openxmlformats-officedocument.wordprocessingml.document

Number of trailing bytes: 18
Dumping trailing bytes to console:
00000000: 7261 6e64 6f6d 5f62 7974 6573 2032 3331  random_bytes 231
00000010: 300a                                     0.
```
Since the trailing bytes are written to stdout in binary format, they can be piped with different commands for further processing.

## File Examples
The folders of the different file types contain example files. The files named 'original.\*' are files without trailing bytes. The files named 'test.\*' are files that contain trailing bytes.
They can be used to verify the function of the program and to get familiar with the output format of the script.
