from bz2 import BZ2Decompressor

def bzip2(file : bytes):
    eof_offset = len(file)
    data = file 
    valid_bz2 = False

    # Consume/Decompress all valid streams
    while data:
        decomp = BZ2Decompressor()

        try:
            res = decomp.decompress(data)
        except OSError:
            if valid_bz2:
                # Leftover data is not a valid bzip2 stream -> our trailing bytes
                eof_offset = len(file) - len(data)
                break
            else:
                # Error in the first stream already -> not a valid bzip2 file
                eof_offset = -1
                break 
            
        valid_bz2 = True

        if not decomp.eof:
            # Compressed data ended before the end-of-stream marker was reached
            eof_offset = -1
            break

        # continue with leftover data; there might be another bz2 stream in the file
        data = decomp.unused_data

    return eof_offset