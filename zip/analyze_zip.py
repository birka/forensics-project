from zipfile import _EndRecData
from io import BytesIO

def zip(file : bytes):
    # Zip files always end with an END OF CENTRAL DIRECTORY RECORD (EOCD)
    # and an optional variable-length comment at the very end.
    # We only extract the EOCD structure using the standard python library zipfile
    eocd =  _EndRecData(BytesIO(file))
    cd_size = eocd[5]
    cd_offset = eocd[6]
    eocd_comment_size = eocd[7]

    # EOF for zip = Central Directory Offset + Central Directory size + size of EOCD record + variable comment size 
    file_end_offset = cd_offset + cd_size + 22 + eocd_comment_size

    return file_end_offset