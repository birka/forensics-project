from io import BytesIO
import py7zr

def sevenZip(file : bytes):
    try:
        with py7zr.SevenZipFile(BytesIO(file), 'r') as archive:
            # header size, is the size of the archive without the compressed data
            header_size = archive.header.size
            # contains information about the compressed streams
            main_streams = archive.header.main_streams

        data_len = 0
        if main_streams:    # Non-Empty Archive
            # Add the length of the compressed data of every data stream
            packsizes = main_streams.packinfo.packsizes
            for size in packsizes:
                data_len += size

        # Byte position of the end_of file
        return header_size + data_len
    except: # File is malformed and couldn't be parsed by the library
        return -1