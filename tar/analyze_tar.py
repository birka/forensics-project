import tarfile
from io import BytesIO

def tar(file : bytes):
    tarf = tarfile.open(fileobj=BytesIO(file))

    # Tape Archive (TAR) format itself is not compressed (it is normally used with
    # gunzip to create a compressed tar.gz archive). We use a standard python
    # library to parse the tar file. We simply get the last member of the archive,
    # which is also the furthest in the file (the one with the highest offset).
    # To get the EOF offset, we add the offset of the last entry + its size
    last_entry = tarf.getmembers()[-1]

    # Note that this calculation might yield trailing zero-bytes on an untouched file.
    # This is because according to the specification, tar archiving programs should
    # create an end-of-archive entry containing of two 512 blocks of zero bytes.
    # However, when reading archives, the program should not fail in case this entry
    # does not exist. We decided to just output any unneeded zero bytes as trailing bytes.
    file_end_offset = last_entry.offset_data + last_entry.size

    return file_end_offset